(function(){
  'use strict';

  angular
    .module('wud.techtest')
    .controller('wudNavbarController',wudNavbarController)

  wudNavbarController.$inject = ['$scope', '$state', '$filter'];
  function wudNavbarController ($scope, $state, $filter) {
    $scope.go = function(item){
      $state.go(item.name);
    };

    $scope.items = $state.get();
    $scope.items = $filter('filter')($scope.items, {abstract: '!true'})

  }
})();