(function(){
	'use strict';

	angular
		.module('wud.techtest')
		.factory('User', User)

	User.$inject = ['$resource'];
	function User($resource){
		return $resource('http://localhost:8000/user');
	}
})();