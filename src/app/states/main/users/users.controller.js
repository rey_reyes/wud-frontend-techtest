(function() {
  'use strict';

  angular
    .module('wud.techtest')
    .controller('UsersController', UsersController);

  /** @ngInject */
  UsersController.$inject = ['User', '$state'];
  function UsersController(User, $state) {
	var vm = this;

	vm.add = function (user) {
		User.save({},user,
			function(){
				$state.reload();
		})
	};

	User.query({},
	  function(success){
	    vm.query = success;
	  });
  }
})();